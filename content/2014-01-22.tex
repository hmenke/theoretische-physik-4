% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 22.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-22}

\chapter{Quantenstatistik}

\begin{table}
	\centering
	\begin{tabular}{>{\raggedright}m{0.3\textwidth}@{\quad$\leftrightarrow$\quad}>{\raggedright}m{0.55\textwidth}}
		\toprule
		%---
		Klassisch
		&
		Quantenmechanisch
		\cr
		%---
		\midrule
		%---
		$(p,q) \in \mathbb{R}^{GN}$
		&
		$\gathered \psi \in \mathcal{H} \equiv \mathcal{L}_2(\mathbb{R}^{3N}) \\ \psi(\bm{x}_1,\dotsc,\bm{x}_N) \quad \int \diff x^{3N} |\psi|^2 = 1 \endgathered$
		\cr
		%---
		\midrule
		Hamiltonfunktion $H(p,q)$
		&
		Hamiltonoperator $H$. Die Quantiesierung verlangt, dass die kanonischen Variablen die kanonischen Vertauschungsrelationen erfüllen: $[x_i,p_j] = \delta_{ij} \frac{\hbar}{\mathrm{i}}$. In der Orts"|darstellung erfüllt der Impulsoperator $p_i = \frac{\hbar}{\mathrm{i}} \partial_{q_i}$.
		\cr
		%---
		\midrule
		$\displaystyle H = \sum_i \frac{\bm{p}_i^2}{2m} + \sum_{i\neq j} V(\bm{x}_i - \bm{x}_j)$
		&
		$\gathered H = \sum_i \frac{\bm{p}_i^2}{2m} + \sum_{i\neq j} V(\bm{x}_i - \bm{x}_j) \\ = \sum_i -\frac{\hbar^2}{2m}\nabla_i^2 + \sum_{i\neq j} V(\bm{x}_i - \bm{x}_j)\endgathered$\par Es existiert eine vollständige orthonormierte Basis in $\mathcal{H}$ $\{\psi_n\}$ mit $E_n \psi_n = H \psi_n$. Jeder beliebige Zustand kann ausgedrückt werden durch das Superpositionsprinzip $\psi = \sum_n c_n \psi_n$.
		\cr
		\bottomrule
	\end{tabular}
	\caption{Vergleich zwischen klassischer Mechanik und Quantenmechanik.}
\end{table}

\minisec{Braket-Notation}

Es gilt
%
\begin{align*}
	\ket{n} &\in \mathcal{H} \\
	E_n \ket{n} &= H \ket{n} \\
	\ket{\psi} &= \sum_n c_n \ket{n}
\end{align*}

In der Ortsdartellung
%
\begin{align*}
	\psi(\bm{x}_1,\dotsc,\bm{x}_N) = \braket{\bm{x}_1,\dotsc,\bm{x}_N|\psi}
\end{align*}

Für Messgrößen (Observablen) definieren wir den Erwartungswert
%
\begin{align*}
	\braket{A} &= \braket{\psi|A|\psi} = \int \diff x^{3N} \psi^*(\bm{x}_1,\dotsc,\bm{x}_N) \, A \, \psi(\bm{x}_1,\dotsc,\bm{x}_N)
\end{align*}
%
Der Erwartungswert einer Observable ist das Mittel aus vielen Experimenten.

Fluktuationen um den Erwartungswert (\acct{Varianz}) ist definiert als
%
\begin{align*}
	\sqrt{\braket{(A-\braket{A})^2}} = (\Delta A)
\end{align*}

Die Wahrscheinlichkeit Teilchen $1$ am Ort $\bm{x}_1$, Teilchen $2$ am Ort $\bm{x}_2$, etc.\ zu finden ist
%
\begin{align*}
	|\psi(\bm{x}_1,\dotsc,\bm{x}_N)|^2
\end{align*}

\begin{example}
	Wählen wir als Observable die Energie: $A = H$.
	%
	\begin{align*}
		\ket{\psi} &= \sum_n c_n \ket{n} \\
		\braket{H}
		&= \braket{\psi|H|\psi} \\
		&= \sum_{n,n'} c_{n'}^* c_n \braket{n'|H|n} \\
		&= \sum_{n,n'} c_{n'}^* c_n E_n \braket{n'|n} \\
		&= \sum_{n,n'} c_{n'}^* c_n E_n \delta{n,n'} \\
		&= \sum_{n} |c_n|^2 E_n
	\end{align*}
	%
	Wir sehen, dass $|c_n|^2$ die Wahrscheinlichkeit ist, den Eigenwert $E_n$ zu messen.

	$c_n$ ist die Amplitude und im Allgemeinen komplex und erlaubt Intereferenzen.
\end{example}

Bei einer Messung kollabiert die Wellenfunktion, d.\,h.\ wenn wir den Energieeigenwert $E_n$ messen, ist das System danach im Zustand $\ket{n}$.

Als Messwerte bekommen wir Eigenzustände der Observablen $A$ und die Wellenfunktion kollabiert auf den Eigenzustand.

Wenn wir den Messwert nicht ablesen bekommen wir den \acct{Dichteoperator}
%
\begin{align*}
	\varrho = \sum_n |c_n|^2 \ket{n}\bra{n}
\end{align*}

Jeder Zustand $\ket{\alpha}$ ist eine Superposition der Basis $\ket{n}$.
%
\begin{align*}
	\ket{\alpha} = \sum_n c_n^\alpha \ket{n}
\end{align*}

In Analogie zur Dichtefunktion $\varrho(p,q)$ in der klassischen statistischen Mechanik müssen wir das Konzept des Dichteoperators einführen.
%
\begin{align*}
	\varrho
	&= \sum_\alpha p_\alpha \ket{\alpha}\bra{\alpha} \\
	&= \sum_{n,n',\alpha} p_\alpha (c_{n'}^\alpha)^* c_n^\alpha \ket{n}\bra{n'}
\end{align*}

Falls $p_\alpha = 0$ außer für ein $\alpha$, so gilt
%
\begin{align*}
	\varrho = \ket{\alpha}\bra{\alpha}
\end{align*}

Der Erwartungswert für gemischte Zustände ist
%
\begin{align*}
	\braket{A} &= \sum_\alpha p_\alpha \braket{\alpha|A|\alpha}
\intertext{nun sei $\varrho = \sum_n p_n \ket{n}\bra{n}$}
	\braket{A}
	&= \sum_n p_n \braket{n|A|n} \\
	&= \sum_n p_n \braket{n|\mathds{1} A|n} \\
	&= \sum_{n,n'} p_n \braket{n|n'}\braket{n'|A|n} \\
	&= \sum_{n'} \left\langle n' \middle| \sum_n p_n \ket{n}\bra{n} A \middle| n'\right\rangle \\
	&= \sum_{n'} \braket{n'|\varrho A|n'} \\
	&= \tr(\varrho A)
\end{align*}
%
Der Erwartungswert wird zu
%
\begin{align*}
	\braket{A} &= \tr(\varrho A) = \tr(A \varrho) \\
	\tr \varrho &= 1 \\
	\tr \varrho^2 &\leq 1
\end{align*}

\begin{center}
	\tikz[every path/.style={thick},baseline=(X.base)]{\node[draw=red,rectangle,rounded corners] (X) {In der Quantenstatistik werden wir $\varrho$ nicht normieren.};}
\end{center}

Allgemeine Bedingung an $\varrho$:
%
\begin{itemize}
	\item $\varrho \in \{\text{Spurklasse}\}$
	\item $\varrho$ selbstadjungiert, $\varrho = \varrho^\dag$
	\item $\varrho$ ist positiv $\braket{\beta|\varrho|\beta} \geq 0$
\end{itemize}

Welcher Dichteoperator beschriebt unser thermodynamisches Gleichgewicht.
