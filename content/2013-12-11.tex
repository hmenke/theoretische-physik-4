% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 11.12.2013
% TeX: Henri

\renewcommand{\printfile}{2013-12-11}

\minisec{Boltzmann-Verteilung} \index{Boltzmann-Verteilung}

\begin{align*}
	f_0(\bm{p}) = n \left( \frac{\lambda}{h} \right)^3 \exp\left[ - \frac{(\bm{p} - \bm{p}_0)^2}{2 m \kB T} \right]
\end{align*}
%
aus der Bedingung, dass $H(t)$ maximal ist.
%
\begin{itemize}
	\item $p V = N \kB T$
	\item $U = \dfrac{3}{2} N \kB T$
\end{itemize}

Der Wert von $H$ im Equilibrium entspricht der Entropie
%
\begin{align*}
	S = N \kB \log\frac{T^{3/2} V}{N} + \const \; , \quad \sim - H
\end{align*}

Wir betrachten $N$ Teilchen und verteilen diese zufällig auf dem Phasenraum. Dazu unterteilen wir den Phasenraum in $K$ kleine Boxen mit Volumen $w = \Delta p^3 \Delta q^3$.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (2,0) node[below] {$q$};
		\draw[->] (0,-0.3) -- (0,1) node[left] {$p$};
		\node[draw,rectangle,minimum size=0.4cm,label={above:$N_i$}] at (1,0.5) {.};
	\end{tikzpicture}
	\caption{Der Phasenraum ist aufgebaut aus $K$ Boxen wie die in der Abbildung, in denen wir die Teilchen platzieren.}
	\label{fig:2013-12-11-1}
\end{figure}

$N_i$ ist die Anzahl der Teilchen in diesem Phasenraumvolumen. Damit lautet die Einteilchenverteilungsfunktion wie folgt
%
\begin{align*}
	f_i = \frac{N_i}{w}
\end{align*}
%
Die Verteilung muss die Teilchenzahl erhalten und die Energie
%
\begin{align*}
	N &= \sum_{i=1}^{K} N_i \\
	E &= \sum_{i=1}^{K} N_i \frac{p_i^2}{2m}
\end{align*}

Es gibt $K^N$ Möglichkeiten die Teilchen auf die Boxen zu verteilen\footnote{Viele verletzen jedoch die Energieerhaltung}. Da die Teilchen nicht unterscheidbar sind gibt es viele Möglichkeiten eine Verteilung $\{ N_i \}$ herzustellen. Wir nennen die Anzahl der Möglichkeiten
%
\begin{align*}
	\Omega(\{ N_i \}) = \frac{N!}{N_1! N_2! \ldots N_k!} g_1^{N_1} \ldots g_k^{N_k}
\end{align*}

Die Größe $\Omega$ wird extensiv durch logarithmieren:
%
\begin{align*}
	\ln \Omega(\{N_i\})
	&= \ln N! - \sum_{i=1}^{K} \ln N! + \sum_{i=1}^{K} \ln g_i^{N_i}
\intertext{Mit der Formel von Stirling}
	&= N \ln N - \sum_{i=1}^{K} N_i \ln N_i + \sum_{i=1}^{K} N_i \ln g_i
\end{align*}

\minisec{Alternative Herleitung}

Wir extremieren $\ln \Omega$ unter den Nebenbedinungen, dass $\sum_i N_i = N$ (Teilchenzahlerhaltung) und $\sum_i N_i \frac{p_i^2}{2m} = E$ (Energieerhaltung) durch Variation nach $N_i$, wobei $\alpha$, $\beta$ die zu den Nebenbedingungen gehörenden Lagrange Parameter sind:
%
\begin{gather*}
	\begin{aligned}
		\delta\left[ \ln\Omega - \alpha \sum_i N_i - \beta \sum_i N_i \frac{p_i^2}{2m} \right] &= 0 \\
		- (\ln N_i + 1) + \ln g_i - \alpha - \beta \frac{p_i^2}{2m} &= 0 \; , \quad \forall N_i \\
	\end{aligned} \\
	\begin{aligned}
		N_i^0 &= g_i \exp\left( -\alpha - \beta \frac{p_i^2}{2m} - 1 \right) \\
		&= C \exp\left( -\beta \frac{p_i^2}{2m} \right)
	\end{aligned}
\end{gather*}
%
Die Boltzmannverteilung ist gerade die wahrscheinlichste Verteilung unter den Nebenbedingungen.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$N_i/N$};
		\draw[->] (0,-0.3) -- (0,2.5) node[left] {$p(N_i)$};
		\draw (2,0.1) -- (2,-0.1) node[below] {$N_i^0/N$};
		\draw[MidnightBlue] plot[smooth,domain=0:3.8] (\x,{2*exp(-2*(\x-2)^2)});
		\draw[<->] ({-2*exp(1)/9+2},1) -- ({2*exp(1)/9+2},1);
	\end{tikzpicture}
	\caption{Maxwell-Boltzmann-Verteilung}
	\label{fig:2013-12-11-2}
\end{figure}

Die interessante Größe ist die Breite der Verteilungsfunktion, bzw. die Varianz:
%
\begin{align*}
	\braket{N_i^2} - \braket{N_i}^2
	&= g_i \frac{\partial}{\partial g_i} \braket{N_i} = N_i^0
\intertext{weil}
	\braket{N_i} &= \frac{\sum_{\{N_i\}} \Omega(\{N_i\}) P(\{N_i\}) N_i}{\sum_{\{N_i\}} \Omega(\{N_i\}) P(\{N_i\})}
\end{align*}

Die Breite der Verteilungsfunktion:
%
\begin{align*}
	\left[ \Braket{\frac{N_i^2}{N^2}} - \Braket{\frac{N_i}{N}}^2 \right]^{1/2} = \frac{1}{\sqrt{N}} \left( \frac{N_i^0}{N} \right)^{1/2}
\end{align*}
%
Für viele Teilchen $N \to \infty$ erhalten wir immer die Maxwell-Boltzmann-Verteilung.
%
%\begin{align*}
%	P(\{N_i\} = \delta_{E,\sum_{i} N_i \frac{p_i^2}{2m}} \delta_{N,\sum_{i} N_i}
%\end{align*}

\section{Relaxation und Transport}

Die \acct{Boltzmann-Transport-Gleichung}
%
\begin{align*}
	\mathcal{D} f(\bm{p},\bm{r},t) = \partial_t f + \bm{v} \nabla_\bm{r} f + \bm{F} \nabla_\bm{p} f = \partial_t f |_\textnormal{Stöße}
\end{align*}
%
ist im allgemeinen nicht lösbar. Ein wichtiges Konzept ist das lokale Gleichgewicht, beschrieben durch die lokale Boltzmann-Verteilung $f_{\ell 0}$.
%
\begin{align*}
	f_{\ell 0} = n(\bm{r}) \frac{1}{[ 2 \pi m T(\bm{r}) \kB ]^{3/2}} \exp\left[ - \frac{(\bm{p} - \bm{p}_0(\bm{r}))^2}{2 m \kB T(\bm{r})} \right]
\end{align*}
%
$n(\bm{r})$, $T(\bm{T})$ und $\bm{p}_0(\bm{r})$ sind Gleichgewichtsparameter. $f_{\ell 0}$ ist aber keine Lösung der Boltzmann-Transport-Gleichung, da
%
\begin{align*}
	\mathcal{D} f_{\ell 0} &\neq 0 \\
	\partial_t f_{\ell 0} |_\textnormal{Stöße} &= 0
\end{align*}
%
Wir konzentrieren uns im wesentlichen auf 2 Lösungen:
\begin{itemize}
	\item Relaxation: Homogenes System ohne Treiber. Wie relaxiert $f$ ins Gleichgewicht $f_{\ell 0}$?
	\item Stationärer Transport: Treiber im System und das Gleichgewicht des getriebenen Systems.
\end{itemize}

\subsection{Linearisierung}

Betrachte die Variation (im Stoßterm)
%
\begin{align*}
	f = f_{\ell 0} (1 + \psi) \; , \quad \text{mit $\psi \ll 1$}
\end{align*}

d.h. $\psi$ ist ein kleine Störung. Dann lautet der Stoßterm
%
\begin{align*}
	\partial_t f |_\textnormal{Stöße}
	&= - f_{\ell 0} \int \diff^3 p_1 \diff^2 p_1' \diff^3 p'\ w_{\bm{p}',\bm{p}_1',\bm{p},\bm{p}_1} f_{\ell 0}(p_1) [\psi(\bm{p}) + \psi(\bm{p}_1) - \psi(\bm{p}') - \psi(\bm{p}_1')] \\
	&= f_{\ell 0} L \psi
\end{align*}
%
wobei $L$ ein linearer Operator ist.