% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 18.12.2013
% TeX: Henri

\renewcommand{\printfile}{2013-12-18}

Die Größen $j_i$, $\Pi_{ik}$, $e$ und $\tau_i$ sind durch die Verteilungsfunktion $f(p,\bm{r},t)$ bestimmt. Die Lösung der Boltzmann-Gleichung lautet
%
\begin{align*}
	f_{\ell 0} - \tau \mathcal{D} f_{\ell 0}
\end{align*}
%
In $f_{\ell 0}$ sind die lokalen Größen $n(\bm{r},t)$, $\tau(\bm{r},t)$ und $\bm{u}(\bm{r},t)$ vertreten. Wir separieren die konvektive Strömung $\bm{u}(\bm{r},t)$ aus des Größen:
%
\begin{itemize}
	\item
		\begin{itemalign}
			j_k = n \bm{u}_k
		\end{itemalign}
	\item
		\begin{itemalign}
			e = \frac{m n \bm{u}^2}{2} + q
		\end{itemalign}
	\item
		\begin{itemalign}
			\Pi_{ik} = m n u_i u_k + p_{ik}
		\end{itemalign}
	\item
		\begin{itemalign}
			\varepsilon_i = \frac{m n}{2} \bm{u}^2 u_i + q u_i + u_k p_{ki} + w_i
		\end{itemalign}
	\item
		\begin{itemalign}
			p_{ik} = m \int \diff^3 p\ v_i v_k f|_{\bm{u} = 0}
		\end{itemalign}
	\item
		\begin{itemalign}
			w_i = \frac{m}{2} \int \diff^3 p\ \bm{v}^2 v_i v f|_{\bm{u} = 0}
		\end{itemalign}
\end{itemize}

In nullter Ordnung hatten wir $f = f_{\ell 0}$ gesetzt und so erhalten
%
\begin{itemize}
	\item
		\begin{itemalign}
			w_i = 0
		\end{itemalign}
	\item
		\begin{itemalign}
			p_{ik} = \delta_{ik} p = \delta_{ik} n \kB T
		\end{itemalign}
\end{itemize}

Einsetzen der Größen $j$, $e$, $\tau_i$, $\Pi_{ik}$
%
\begin{align*}
	\partial_t n + \partial_i (n u_i) &= 0 \\
	m \partial_t (n u_k) + \partial_i (m n u_i u_k + \delta_{ik} p) &= m F_k \\
	\partial_t \left( \frac{mn}{2} u^2 + \frac{3}{2} n \kB T \right) + \partial_i \left[ \left( \frac{mn}{2} u^2 + \frac{5}{2} n \kB T \right) u_i \right] &= n F_i u_i
\end{align*}
%
mit dem Ableitungsoperator
%
\begin{align*}
	D_t \equiv \partial_t + u_i \partial_i
\end{align*}
%
$D_t$ wird auch substantielle Ableitung genannt.
%
\begin{align*}
	\partial_t n + \partial_i (n u_i) = \underbrace{\partial_t n + u_i \partial_i n}_{D_t n} + n \partial_i u_i = D_t n + n (\nabla \cdot \bm{u}) &= 0 \\
	\cancel{$m u_i \partial_t n$} +
	\tikz[remember picture,baseline=(XX.base),every path/.style={}]{\node[blue,inner sep=0pt] (XX) {$m n \partial_t u_k$};} +
	\cancel{$m u_k \partial_i (n u_i)$} +
	\tikz[remember picture,baseline=(XY.base),every path/.style={}]{\node[blue,inner sep=0pt] (XY) {$m n u_i \partial_i u_k$};} +
	\partial_k p &= m F_k \\[0.5\baselineskip]
	\tikz[remember picture,baseline=(XZ.base),every path/.style={}]{\node[blue,inner sep=0pt] (XZ) {$m n D_t u_k$};} +
	\partial_k p &= m F_k
\end{align*}
\begin{tikzpicture}[remember picture,overlay,every path/.style={}]
	\draw[blue] (XX) edge[out=-90,in=150,->] (XZ);
	\draw[blue] (XY) edge[out=-90,in=150,looseness=2,->] (XZ);
\end{tikzpicture}
%
Wir erhalten also folgende 0. Ordnung Gleichungen:
%
\begin{align*}
	D_t n + n (\nabla \cdot \bm{u}) &= 0 \\
	m n D_t \bm{u} + \nabla p &= n \bm{F} \\
	\frac{3}{2} D_t T + T \nabla \cdot \bm{u} &= 0
\end{align*}
%
Sie beschreiben ein ideales Fluidum. Die geleistete Arbeit geht in kinetische Energie über. Der Transport ist adiabatisch.
%
\begin{align*}
	n \nabla \cdot \bm{u} &= - D_t n \\
	\frac{3}{2} D_t T + T \nabla \cdot \bm{u} &= 0 \\
	\frac{3}{2} \frac{D_t T}{T} - \frac{D_t n}{n} &= 0 \\
	D_t \ln T^{3/2} - D_t \ln n &= D_t \left[ \ln \frac{T^{3/2}}{n} \right] = D_t \left( \frac{s}{n} \right) = 0
\end{align*}
%
Die Entropie pro Teilchen ist
%
\begin{align*}
	S &= N \kB \ln \frac{T^{3/2} V}{N} \\
	\frac{S}{N} &= \frac{s}{n} = \kB \ln \frac{T^{3/2}}{n}
\end{align*}

\section{Erste Ordnung, Navier-Stokes-Gleichung}

Um Dissipation der konvektiven Strömung und Umwandlung von konvektiver Strömung in Wärme zu erhalten müssen wir zur ersten Ordnung übergehen.
%
\begin{align*}
	f = f_{\ell 0} - \tau \mathcal{D} F_{\ell 0}
\end{align*}
%
Der Term $-\tau \mathcal{D} f_{\ell 0}$ hat keinen Einfluss auf $n$, $\bm{j}$ und $q$, da Stöße diese Größen nicht relaxieren.

Nach langer Rechnung folgt
%
\begin{align*}
	p_{ik} =
	\left[ p + \frac{2}{3} \eta (\nabla \cdot \bm{u}) \right] \delta_{ik}
	- \eta \left( \partial_i u_k + \partial_k u_i \right)
\end{align*}
%
mit $\eta = n \kB T \tau = p \tau$, dem Viskositätskoeffizienten.
%
\begin{align*}
	w_i = - \kappa \partial_k T \; , \quad \bm{w} = - \kappa \nabla T
\end{align*}
%
mit \[ \kappa = n \kB T \frac{\tau}{m} c_p \; , \quad c_p = \frac{5}{2} \kB. \]
Es gilt die Beziehung
\[ \frac{\kappa}{\eta} = \frac{c_p}{m}. \]
Wir führen die Matrix ein
\[ [\hat{p}]_{ik} = - \eta \left[ \left( \partial_i u_k + \partial_k u_i \right) - \frac{2}{3} (\nabla \cdot \bm{u} \delta_{ik} \right] \]
dann erhalten wir
%
\begin{align*}
	D_t n + n \nabla \cdot \bm{u} &= 0 \\
	m n D_t \bm{u} + \nabla p &= n \bm{F} - \nabla \cdot \hat{p} \\
	n \kB \left[ \frac{3}{2} D_t T + T \nabla \cdot \bm{u} \right] &= \nabla (\kappa \nabla T) - (\hat{p} \cdot \nabla) \cdot \bm{u}
\end{align*}
%
Sowohl $\kappa$ als auch $\eta$ hängen von der Dichte und der Temperatur ab. Diese Gleichungen beschreiben das Verhalten eines realen Fluidum. Für $\bm{u} = 0$ erhalten wir die Wärmeleitungsgleichung.
%
\begin{align*}
	n \kB \frac{3}{2} \partial_t T &= \nabla \cdot ( \kappa \nabla T ) \approx \kappa \nabla^2 T \\
	\partial_t T &= \frac{\kappa}{n c_v} \nabla^2 T
\end{align*}

Verschiedene Typen von Strömungen:
%
\begin{itemize}
	\item adiabatisch
		\begin{align*}
			D_t \left( \frac{s}{n} \right) = 0
		\end{align*}
	\item stationär 
		\begin{align*}
			\partial_t \bm{u} = 0
		\end{align*}
	\item rotierend
		\begin{align*}
			\nabla \wedge \bm{u} \neq 0
		\end{align*}
	\item nicht-rotierend
		\begin{align*}
			\nabla \wedge \bm{u} = 0
		\end{align*}
	\item potential
		\begin{align*}
			\bm{u} = - \nabla \phi
		\end{align*}
		Folgt aus $\nabla \wedge \bm{u} = 0$.
\end{itemize}

Außerdem
%
\begin{itemize}
	\item Inkompressible Flüssigkeiten: $n = \const$
	\item Inkompressibel, stationär und rotationsfrei
		\begin{align*}
			\nabla \cdot \bm{u} &= 0 \\
			\bm{u} &= - \nabla \phi \\
			\leadsto \nabla^2 \phi &= 0
		\end{align*}
\end{itemize}
