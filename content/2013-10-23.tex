% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 23.10.2013
% TeX: Henri

\renewcommand{\printfile}{2013-10-23}

\section{Versuch von Joule-Kelvin} \index{Versuch von!Joule-Kelvin}

Der Versuch wird adiabatisch durchgeführt bei $\delta Q = 0$. Das Volumen das Gases ändert sich dabei von $V_{AA'}$ zu $V_{BB'}$, wobei eine Wattebausch ein Druckgefälle aufrecht erhält. Der Versuchsaufbau ist in Abbildung~\ref{fig:2013-10-23-1} abgebildet.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\begin{scope}[yshift=2cm]
			\draw (0,0) -- (3.5,0);
			\draw (0,2) -- (3.5,2);
			\filldraw (0.5,0) -- (0.5,2) -| (0.3,1.1) -- (0,1.1) node[left] {$K$} |- (0.3,0.9) -- (0.3,0) -- cycle;
			\filldraw (3,0) -- (3,2) -| (3.2,1.1) -- (3.5,1.1) node[right] {$K'$} |- (3.2,0.9) -- (3.2,0) -- cycle;
			\filldraw[pattern=crosshatch] (1.5,0) rectangle (2,2);
			\node at (1,1) {$p$};
			\node at (2.5,1) {$p'$};
			\node[above] at (0,2)   {$A$};
			\node[above] at (1.5,2) {$A'$};
			\node[above] at (2,2)   {$B$};
			\node[above] at (3.5,2) {$B'$};
		\end{scope}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$x$};
		\draw[->] (0,-0.3) -- (0,2) node[left] {$p$};
		\draw[dotted] (0.5,0) -- (0.5,2);
		\draw[dotted] (1.5,0) -- (1.5,2);
		\draw[dotted] (2,0) -- (2,2);
		\draw[dotted] (3,0) -- (3,2);
		\draw (0.5,1.5) -- (1.5,1.5) -- (2,1) -- (3,1);
	\end{tikzpicture}
	\caption{Versuchsaufbau von Joule-Kelvin mit Verlauf des Drucks über die beiden Kammern}
	\label{fig:2013-10-23-1}
\end{figure}

Der Kolben $K$ drückt das Gas mit konstanter Kraft ($p = \const$) durch den Wattebausch. Der Kolben $K'$ weicht mit konstanter Kraft zurück ($p' = \const$).

Im stationären Prozess sind $p(x)$ und $T(x)$ stationär, aber nicht-trivial. Die Energiebilanz für diesen adiabatischen Prozess bei $\delta Q = 0$ ist
%
\begin{equation}
	U - U' = - p V + p' V'
	\tag{*}
	\label{eq:2013-10-23-stern1}
\end{equation}
%
wobei $V = V_{AA'}$ und $V' = V_{BB'}$ die von den Kolben $K$ und $K'$ überstrichenen Volumina sind. Umsortieren liefert
%
\begin{equation*}
	H = U + p V = U' + p' V' = H'
\end{equation*}

$H$ ist die \acct{Enthalpie}. Diese bleibt während des Prozesses erhalten. Die Enthalpie beschreibt den durch den Zylinder strömenden Energiefluss.

\minisec{Spezialfall: Ideales Gas}

Wir setzen die Zustandsgleichung in \eqref{eq:2013-10-23-stern1} ein und erhalten
%
\begin{equation*}
	\boxed{
		U - U' = n R (T - T')
	}
\end{equation*}

Im Experiment zeigt sich, dass $T - T' \approx 0$. Für ein ideales Gas sollte jedoch $T - T' = 0$ sein. Es folgt, dass
$U_\textrm{ideales Gas}(T)$ unabhängig von $V$ ist.

\section{Adiabatisch-reversible Expansion}

Wir betrachten adiabatisch-reversible Prozesses der Gasexpansion als auch Gaskompression. Hierzu wird ein thermisch isoliertes Zylinder-Kolben-System verwendet mit dem Aufbau in Abbildung~\ref{fig:2013-10-23-2}.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (3.5,2) -- (1,2) node[below right] {$T$} -- (1,0) node[above right] {$V$} -- (3.5,0);
		\filldraw (3,0) -- (3,2) -| (3.2,1.1) -- (3.5,1.1) node[right] {$K$} |- (3.2,0.9) -- (3.2,0) -- cycle;
		\draw[->] (2,1) -- node[above] {$p'$} (3,1);
		\node[above] at (3,2) {$\delta Q = 0$};
	\end{tikzpicture}
	\caption{Versuchsaufbau zur adiabtisch-reversiblen Expansion}
	\label{fig:2013-10-23-2}
\end{figure}

Mit $\delta q = 0$ ergibt der erste Hauptsatz
%
\begin{equation*}
	\delta w = p \diff v
\end{equation*}

Die kalorische Gleichung für ein ideales Gas lautet
%
\begin{equation*}
	\diff u = c_v \diff T
\end{equation*}

Wieder in den ersten Hauptsatz eingesetzt erhält man
%
\begin{equation*}
	\implies
	c_v \diff T = \diff u = \delta q - \delta w = p \diff V
\end{equation*}

Setzt man nun die Zustandsgleichung des idealen Gases ein
%
\begin{gather*}
	\diff T = \frac{p \diff V + v \diff p}{R} \\
	\implies (c_v + R) p \diff v + c_v v \diff p = 0
\end{gather*}
%
oder mit $c_v + R = c_p$
%
\begin{equation*}
	\frac{\diff p}{p} + \frac{c_p}{c_v} \frac{\diff v}{v} = 0
\end{equation*}
%
Integration liefert
%
\begin{align*}
	\log p + \log v^{c_p/c_v} &= \const \\
	p v^{c_p/c_v} &= \const
\end{align*}

Es gilt \[ \frac{c_p}{c_v} = \gamma \] wobei für ein monoatomares Gas $\gamma = 5/3$ gilt.

Umgeschrieben auf Variablenpaare $p,T$ erhalten wir \acct*{Adiabatengleichungen} \index{Adiabatengleichung} $v,T$
%
\begin{align*}
	\begin{array}{r@{{}={}}l@{\; , \quad}r@{{}={}}l@{\; , \quad}r}
		p v^{\gamma} & \const & p v^{5/3} & \const & \to p,v \\
		p^{\frac{1-\gamma}{\gamma}} T & \const & p^{-2/3} T & \const & \to p,T \\
		v^{\gamma - 1} T & \const & v^{2/3} T & \const & \to v,T \\
	\end{array}
\end{align*}

Im Vergleich zur Isotherme $p v = \const$ fällt die Adiabate steiler ab, siehe Abbildung~\ref{fig:2013-10-23-3}. Dies wird beim Carnot-Prozess verwendet.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$V$};
		\draw[->] (0,-0.3) -- (0,3) node[left] {$p$};
		\draw plot[samples=41,domain=0.333:4] (\x,{1/\x});
		\draw[dashed] plot[samples=41,domain=1:4] (\x,{1/\x^(5/3)});
		\draw[dashed] plot[samples=41,domain=0.5:1] (\x,{\x^(-5./3.)});
	\end{tikzpicture}
	\caption{Im $p,V$-Diagramm fällt eine Adiabate (gestrichelt) steiler ab, als eine Isotherme (durchgezogen).}
	\label{fig:2013-10-23-3}
\end{figure}

\begin{notice}[Beachte:]
	Die adiabatisch-reversible Expansion des Gases liefert Arbeit, wobei dem Gas via Abkühlung die innere Energie $c_v \diff T$ entzogen wird. Diese innere Energie wurde dem Gas irgendwann zugeführt.
\end{notice}

\section{Wärme und Entropie des idealen Gases}

Betrachte die Definition
%
\begin{equation*}
	\delta q = \diff u + p \diff v
\end{equation*}
%
mit $\diff u = c_v \diff T$ und $p = RT/v$.
%
\begin{equation*}
	\delta q = c_v \diff T + R \frac{T}{v} \diff v
\end{equation*}

$\delta q$ ist kein vollständiges Differential, aber $\delta q/T$:
%
\begin{equation*}
	\diff s = \frac{\delta q}{T} = c_v(T) \frac{\diff T}{T} + \frac{R}{v} \diff v
\end{equation*}
%
für $c_v = \const$:
%
\begin{align*}
	s(T,v) &= \int_{T_0,v_0}^{T,v} \diff s = c_v \log \frac{T}{T_0} + R \log \frac{v}{v_0} \\
	\implies S(T,V) &= n s(T,v) = n R \left[ \log\left( \frac{T}{T_0} \right)^{3/2} + \log\left( \frac{V}{V_0} \right) \right] \; , \quad \text{für $s_0 = 0$}
\end{align*}
%
Der Faktor $1/T$ ist der integrierende Faktor des Differentials $\delta q$. Wir haben eine neue Zustandsgröße erhalten: Die \acct{Entropie} $S$.

Die Wärme lässt sich ausdrücken als $\delta q = T \diff s$ womit der Energiesatz wird zu
%
\begin{equation*}
	\boxed{
		\diff u = T \diff s - p \diff v
	}
\end{equation*}

Weiter ist $u = u(s,v)$, $\partial_s u|_v = T$, $\partial_v u|_s = -p$ und \[ \frac{\partial^2 u}{\partial v \partial s} = \frac{\partial^2 u}{\partial s \partial v} \]

\chapter{Zweiter Hauptsatz der Thermodynamik}\label{chap:4}

Der zweite Hauptsatz bildet den Kern der Thermodynamik. Er spezifiziert, welche (energetisch erlaubten) Prozesse in der Natur vorkommen dürfen.
Der zweite Hauptsatz beruht auf Erfahrungen und wird nicht bewiesen. Er ist somit ein Axiom der Thermodynamik --- Die Zurückführung auf einfache, scheinbar selbstverständliche Prinzipien muss genügen. Dies wird durch die Postulate von Clausius und Kelvin erreicht, in Kombination mit der Carnot-Maschine.
%
\begin{description}
	\item[Clausius:] Wärme kann nicht von selbst aus einem niederen zu einem höheren Temperaturniveau übergehen.
	
	\item[Kelvin:] Es ist unmöglich, fortlaufend Arbeit zu erzeugen durch bloße Abkühlung eines einzelnen Körpers.
\end{description}

Gemäß Kelvin gibt es keine Wärmemaschine mit nur einem Wärmereservoir. Eine Wärmemaschine erzeugt immer eine Abwärme.

\section{Kreisprozess von Carnot}

Wir untersuchen eine zyklisch arbeitende Maschine mit einem beliebigen homogenen Arbeitsmedium, dessen Zustand sich durch die mechanischen Variablen $p$ und $V$ beschreiben lässt.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (4,0) node[below] {$V$};
		\draw[->] (0,-0.3) -- (0,3) node[left] {$p$};
		%
		\draw[dashed] plot[domain=0.5:1.14575] (\x,{1/\x+1.2});
		\draw[dashed] plot[domain=2.24152:3.5] (\x,{1/\x+1.2}) node[right] {$\vartheta_2$};
		\draw[dashed] plot[domain=0.5:1.63269] (\x,{1/\x+0.2});
		\draw[dashed] plot[domain=2.94872:3.5] (\x,{1/\x+0.2}) node[right] {$\vartheta_1$};
		\draw plot[domain=1:2] (\x,{exp(ln(\x-0.5)*(-5/3))});
		\draw plot[domain=2:3.5] (\x,{exp(ln(\x-1.5)*(-5/3))});
		%
		\draw[->,DarkOrange3,dashed] plot[domain=1.14575:2.24152] (\x,{1/\x+1.2});
		\draw[<-,DarkOrange3,dashed] plot[domain=1.63269:2.94872] (\x,{1/\x+0.2});
		\draw[<-,MidnightBlue] plot[domain=1.14575:1.63269] (\x,{exp(ln(\x-0.5)*(-5/3))});
		\draw[->,MidnightBlue] plot[domain=2.24152:2.94872] (\x,{exp(ln(\x-1.5)*(-5/3))});
		%
		\draw node[dot,label={above right:$1$}] at (1.14575,2.07279) {};
		\draw node[dot,label={above right:$2$}] at (2.24152,1.64613) {};
		\draw node[dot,label={above right:$3$}] at (2.94872,0.539131) {};
		\draw node[dot,label={below:$4$}] at (1.63269,0.81248) {};
		%
		\node at (1.9,1.2) {$W$};
	\end{tikzpicture}
	\caption{$p,V$-Diagramm der Carnot-Maschine.}
\end{figure}

Die thermische Variable $\vartheta$ folgt aus der Zustandsgleichung des Mediums.
Der Kreisprozess besteht aus den Isothermen $I_{12}$, $I_{34}$ und den Adiabaten $A_{23}$ und $A_{41}$.

Entlang der Isothermen $I_{12}/I_{34}$ werden dem Wärmereservoir bei den Temperaturen $\vartheta_1/\vartheta_2$ die Wärmemengen $Q_1/Q_2$ entzogen, woraus sich die Wärmezufuhr pro Zyklus ergibt als
%
\begin{equation*}
	\oint \delta Q = Q_1 - Q_2
\end{equation*}
%
Die geleistete Arbeit pro Zyklus ist
%
\begin{equation*}
	\oint \delta W = \oint \diff V p = W
\end{equation*}

Gemäß dem ersten Hauptsatz gilt
%
\begin{equation*}
	W = Q_1 - Q_2
\end{equation*}

Nach Kelvin muss die Wärmemaschine daher Abwärme erzeugen. Der thermische \acct{Wirkungsgrad} ist
%
\begin{equation*}
	\boxed{
		\eta = \frac{W}{Q_1} = 1 - \frac{Q_2}{Q_1}
	}
\end{equation*}
%
Der Wirkungsgrad ist die Fähigkeit einer Maschine Wärme in Arbeit zu verwandeln. Es gilt stets $\eta < 1$.

Die Maschine kann auch als Kältemaschine bzw.\ Wärmepumpe benutzt werden. Der Wirkungsgrad der Wärmepumpe
%
\begin{equation*}
	\eta = \frac{Q_1}{W} = \frac{1}{1 - \frac{Q_2}{Q_1}}
\end{equation*}

Eine Kraftmaschine entzieht dem Medium höherer Temperatur die Wärme $Q_1$ und verwandelt diese in Arbeit $W$ und Abwärme $Q_2$, wobei $Q_1 - Q_2 - W = 0$. Für eine Wärmepumpe unterscheidet sich die Bilanz dahingehend, dass $Q_2 + W - Q_1 = 0$ gilt.  Die Wärmepumpe entzieht dem Reservoir bei tiefer Temperatur die Wärme $Q_2$ und führt die Wärme $Q_1$ dem Reservoir höherer Temperatur zu, wobei Arbeit zu leisten ist.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}[scale=0.7]
		\fill[pattern=north east lines] (-1,2) rectangle (1,2.5) node [below right] {$\vartheta_1$};
		\draw (-1,2) -- (1,2);
		%
		\draw (-0.2,2) -- (-0.2,-2);
		\draw (0.2,2) -- (0.2,-2);
		%
		\draw (0,0.2) -- (2,0.2);
		\draw (0,-0.2) -- (2,-0.2);
		%
		\draw[fill=white] (0,0) circle (1);
		%
		\draw[->] (0,1.8) -- node[right=0.2] {$Q_1$} (0,1.2);
		\draw[->] (0,-1.2) -- node[right=0.2] {$Q_2$} (0,-1.8);
		\draw[->] (1.2,0) -- node[below=0.2] {$W$} (1.8,0);
		%
		\draw (-1,-2) -- (1,-2);
		\fill[pattern=north east lines] (-1,-2) rectangle (1,-2.5) node [above right] {$\vartheta_2$};
	\end{tikzpicture}
	\hspace{0.1\textwidth}
	\begin{tikzpicture}[scale=0.7]
		\fill[pattern=north east lines] (-1,2) rectangle (1,2.5) node [below right] {$\vartheta_1$};
		\draw (-1,2) -- (1,2);
		%
		\draw (-0.2,2) -- (-0.2,-2);
		\draw (0.2,2) -- (0.2,-2);
		%
		\draw (0,0.2) -- (2,0.2);
		\draw (0,-0.2) -- (2,-0.2);
		%
		\draw[fill=white] (0,0) circle (1);
		%
		\draw[<-] (0,1.8) -- node[right=0.2] {$Q_1$} (0,1.2);
		\draw[<-] (0,-1.2) -- node[right=0.2] {$Q_2$} (0,-1.8);
		\draw[<-] (1.2,0) -- node[below=0.2] {$W$} (1.8,0);
		%
		\draw (-1,-2) -- (1,-2);
		\fill[pattern=north east lines] (-1,-2) rectangle (1,-2.5) node [above right] {$\vartheta_2$};
	\end{tikzpicture}
	\caption{Vergleich von Kraftmaschine und Wärmepumpe}
\end{figure}

Im weiteren betrachten wir reversible Carnot-Maschinen die, abgesehen von der thermodynamischen Abwärme, keine inneren Verluste aufweisen.

\begin{theorem}[Satz]
	Alle zwischen $\vartheta_1$ und $\vartheta_2$ operierenden (reversiblen) Carnot-Maschinen besitzen unabhängig vom Medium denselben Wirkungsgrad.
	%
	\begin{equation*}
		\eta = 1 - \frac{Q_2}{Q_1}
	\end{equation*}	
\end{theorem}

