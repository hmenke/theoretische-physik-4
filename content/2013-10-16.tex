% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 16.10.2013
% TeX: Henri

\renewcommand{\printfile}{2013-10-16}

\chapter{Einführung}

Die Thermodynamik befasst sich mit den makroskopischen Eigenschaften eines Systems im thermodynamischen Gleichgewicht.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[double] (-0.1,-0.1) rectangle (2.1,1.1);
		\draw plot[samples=50,only marks,mark=*,mark size=0.5] ({1+rand},{rnd});
	\end{tikzpicture}
	\caption{Abgeschlossenes System dargestellt durch eine Box mit einer großen Anzahl Atome darin.}
	\label{fig:2013-10-16-1}
\end{figure}

Die Thermodynamik ist bestimmt durch \acct{Zustandsgrößen}. Die messbaren Zustandsgrößen sich zum Beispiel:
%
\begin{itemize}
	\item Der Druck $p$

	\item Die Temperatur $T$

	\item Das Volumen $V$

	\item Die Teilchenzahl $N$

	\item Das chemische Potential $\mu$

	\item Das Magnetfeld $H$

	\item Die Magnetisierung $M$
\end{itemize}

Davon können verschiedene Größen abgeleitet werden:
%
\begin{itemize}
	\item Die innere Energie $U$

	\item Die Entropie $S$

	\item Die freie Energie $F$

	\item Das thermodynamische Potential $\Omega$
\end{itemize}

Die meisten dieser Zustandsgrößen hängen voneinander ab. Wir können also nur wenige festlegen.

\begin{example}
	Ein ideales Gas.

	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\draw (0,0) rectangle (1,1);
			\node[right] at (1,1) {$V$};
			\node[right] at (1,0.5) {$T$};
			\node[right] at (1,0) {$p$};
		\end{tikzpicture}
		\caption{Ein ideales Gas in einer Box mit verschiedenen Zustandsgrößen.}
		\label{fig:2013-10-16-2}
	\end{figure}

	Die \acct*{thermische Zustandsgleichung} \index{thermische Zustandsgleichung !ideales Gas} lautet:
	%
	\begin{equation*}
		f(T,p,V,\ldots) = 0
	\end{equation*}
	%
	Für ein ideales Gas gilt
	%
	\begin{equation*}
		pV = nRT
	\end{equation*}

	Ebenso kann eine \acct*{kalorische Zustandsgleichung} \index{kalorischen Zustandsgleichung!ideales Gas} gefunden werden
	%
	\begin{equation*}
		U(T,V,\ldots)
	\end{equation*}
	%
	Für ein ideales Gas erhält man
	%
	\begin{equation*}
		U = \frac{f}{2} n R T \; , \quad f = 3
	\end{equation*}
\end{example}

Die Zustandsgrößen gliedern sich in \acct*{extensive}\index{Zustandsgrößen!extensive} und \acct*{intensive}\index{Zustandsgrößen!intensive}. Zu den extensiven Größen gehören z.\,B.\ $N$, $V$ oder $U$. Diese skalieren mit der Systemgröße.

Intensive Größen sind $T$, $p$ oder $\mu$. Beim Zusammenschluss von zwei Systemen $\mathcal{S}_1$ und $\mathcal{S}_2$ zu $\mathcal{S}$ schließen sich auch diese Größen zusammen. Zum Beispiel $T_1 \oplus T_2 = T$.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (0,3) -- (0,0) node[left] {Zeit};
		\node[draw,rectangle] at (1,2.5) {$T_1$};
		\node[draw,rectangle] at (2,2.5) {$T_2$};
		\node[draw,rectangle,minimum width=1cm] at (1.5,1.5) {$T_1' \; | \; T_2'$};
		\node[draw,rectangle,minimum width=1cm] at (1.5,0.5) {$T$};
		\node[right] at (3,2.5) {GG};
		\node[right] at (3,1.5) {nGG,Wärmetransport};
		\node[right] at (3,0.5) {GG};
	\end{tikzpicture}
	\caption{Zusammenschluss zweier Systeme}
	\label{fig:2013-10-16-3}
\end{figure}

Eine Zustandsgröße $Z$ besitzt die Eigenschaft, dass sie nur vom Zustand $(A,B)$ abhängt und nicht von der Art und Weise, auf die der Zustand erreicht wird.
%
\begin{gather*}
	\begin{aligned}
		Z_B
		&= Z_A + \int_{\gamma_1} \diff Z \\
		&= Z_A + \int_{\gamma_2} \diff Z
	\end{aligned} \\
	\int_{\gamma_1} \diff Z - \int_{\gamma_2} \diff Z = \oint_{\gamma} \diff Z = 0
\end{gather*}

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (3,0) node[below] {$T$};
		\draw[->] (0,-0.3) -- (0,3) node[left] {$p$};
		\node[dot,label={left:$A$}]  (A) at (1,1) {};
		\node[dot,label={right:$B$}] (B) at (2,2) {};
		\draw[->] (A) .. controls +(0.8,0) .. node[below right] {$\gamma_1$} (B);
		\draw[->] (A) .. controls +(0,1) and +(-0.5,1) .. node[above left]  {$\gamma_2$} (B);
	\end{tikzpicture}
	\caption{Änderung einer Zustandsgröße.}
	\label{fig:2013-10-16-4}
\end{figure}

Beispiele für Größen, die keine Zustandsgrößen sind:
%
\begin{align*}
	\delta W &= - p \diff V \\
	\delta Q &
\end{align*}

\minisec{Differentiale}

Sei $Z = Z(x,y)$, dann gilt
%
\begin{align*}
	\diff Z
	&= \left. \frac{\partial Z}{\partial x} \right|_y \diff x + \left. \frac{\partial Z}{\partial y} \right|_x \diff y \\
	&= X \diff x + Y \diff y
\end{align*}
%
Damit folgt
%
\begin{equation*}
	\frac{\partial X}{\partial y}
	= \frac{\partial^2 Z}{\partial y \partial x} = \frac{\partial^2 Z}{\partial x \partial y} = \frac{\partial}{\partial x} \frac{\partial Z}{\partial y} = \frac{\partial Y}{\partial x}
\end{equation*}
%
Also gilt für jede Zustandsgröße
%
\begin{equation*}
	\frac{\partial X}{\partial y} = \frac{\partial Y}{\partial x}
\end{equation*}

\minisec{Integrabilität}

Betrachten wir nun das Differential $\diff Z = X \diff x + Y \diff y$
%
\begin{gather*}
	\oint \diff Z = 0 \iff \frac{\partial X}{\partial y} = \frac{\partial Y}{\partial x} \\
	= \int_A \frac{\partial X}{\partial y} - \frac{\partial Y}{\partial x} \diff A = 0
\end{gather*}

In drei Dimensionen gilt damit
%
\begin{align*}
	\diff Z
	&= X \diff x + Y \diff y + W \diff w \\
	&= \bm{Z}
	\begin{pmatrix}
		\diff x \\ \diff y \\ \diff z
	\end{pmatrix} \\
	\nabla \wedge \bm{Z} &= 0
\end{align*}

\section{Reversible und irreversible Prozesse}

Man kann den Kolben so langsam bewegen, dass das System zu jedem Zeitpunkt im Gleichgewicht bleibt. Dieser Prozess ist reversibel.

\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw (0,0) rectangle (4,2);
		\draw (0.5,0.5) rectangle (3.5,1.5);
		\node[below left] at (0.5,0.5) {$T$};
		\begin{scope}[xshift=1.5cm,yshift=0.5cm]
			\filldraw (0,1) -- (0,0) node[below] {$V_A$} -| (0.1,0.4) -- (0.5,0.4) |- (0.1,0.6) |- (0,1);
		\end{scope}
		\begin{scope}[xshift=2.5cm,yshift=0.5cm]
			\filldraw (0,1) -- (0,0) node[below] {$V_B$} -| (0.1,0.4) -- (0.5,0.4) |- (0.1,0.6) |- (0,1);
		\end{scope}
		\draw[->] (1.8,0.7) -- (2.4,0.7);
	\end{tikzpicture}
	\caption{Ein Kolben drückt ein Gas zusammen.}
	\label{fig:2013-10-16-5}
\end{figure}

Ein Beispiel für einen irreversiblen Prozess wäre ein Kasten, in dessen Mitte ein Schieber eingebracht ist. Auf der einen Seite des Schiebers befindet sich Gas, auf der anderen Vakuum. Entfernt man den Schieber, so strömt das Gas ins Vakuum. Will man nun den Schieber wieder einbringen, so müsste das Gas wieder auf die eine Seite zurückwandern und das Vakuum auf der anderen Seite wiederherstellen. Dies ist nicht möglich. Der Prozess ist also irreversibel.


Läuft ein Prozess ab mit 
%
\begin{description}
	\item[konstanter Temperatur] so heißt er \acct*{isotherm}. \index{Zustandsänderung!Isothem}

	\item[konstantem Druck] so heißt er \acct*{isobar}. \index{Zustandsänderung!Isobar}

	\item[konstantem Volumen] so heißt er \acct*{isochor}.\index{Zustandsänderung!Isochor}

	\item[$\delta Q = 0$] so heißt er \acct*{adiabatisch}.\index{Zustandsänderung!Adiabatisch}
\end{description}

Die Thermodynamik basiert auf drei Hauptsätzen.

\begin{theorem}[Erster Hauptsatz] \index{Hauptsätze der Thermodynamik!Erster Hauptsatz}
	Jedes thermodynamische System besitzt die charakterisierende Zustandsgröße $U$ (innere Energie). Die innere Energie ändert sich durch die Arbeit, die am System verrichtet wird und die Wärme, die mit der Umgebung ausgetauscht werden kann.
	%
	\begin{equation*}
		\diff U = \delta Q - \delta W
	\end{equation*}
\end{theorem}

Der erste Hauptsatz ist nichts anderes als eine Energieerhaltung.

\begin{theorem}[Zweiter Hauptsatz]\index{Hauptsätze der Thermodynamik!Zweiter Hauptsatz}
	Jedes thermodynamische System besitzt eine Zustandsgröße $S$ (Entropie). Die Änderung der Entropie ist die zugeführte Wärme (in einem reversiblen Prozess) dividiert durch eine geeignet definierte Temperatur.

	In einem abgeschlossenen System nimmt die Entropie immer zu.
\end{theorem}

\begin{theorem}[Dritter Hauptsatz] \index{Hauptsätze der Thermodynamik!Dritter Hauptsatz}
	Die Entropie für ein System bei $T=0$ (absoluter Nullpunkt) ist eine Konstante. \[ S = 0 \]
\end{theorem}


