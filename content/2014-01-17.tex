% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 17.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-17}

\begin{notice}[Erinnerung:]
	Zustandssumme des Großkanonischen Ensembles
	%
	\begin{align*}
		\mathcal{Z} = \sum_{N=0}^{\infty} z^N Z_N(T,V)
	\end{align*}
	%
	mit der \acct{Fugazität} $z = \mathrm{e}^{-\beta \mu}$. Wobei $\mu$ das chemische Potential des Reservoirs ist.
	%
	\begin{align*}
		\frac{Z_{N_2}}{Z_N}
		&= \mathrm{e}^{-\beta [F(T,V-V_1),N-N_1) - F(T,V,N)]} \\
		&\approx \mathrm{e}^{-\beta [p V_1 - \mu N_1]}
	\end{align*}
\end{notice}

Damit lautet die Zustandssumme 
%
\begin{align*}
	\mathcal{Z} &= \sum_{N=0}^{\infty} \int \frac{\diff^{3N}p \diff^{3N}q}{h^{3N} N!} \mathrm{e}^{-\beta [H(p,q) - \mu N]} 
\end{align*}
%
und wir finden das Große Potential $G$:
%
\begin{align*}
	-\Omega(T,V,\mu) &= p V \equiv \kB T \log \mathcal{Z}(T,V,z)
\end{align*}

Mit
%
\begin{align*}
	1 &= \sum_{N_1 = 0}^{\infty} \int \diff p_1 \diff q_1\ \varrho(p_1,q_1,N_1) = \sum_{N_1 = 0}^{\infty} \int \diff p_1 \diff q_1\ \frac{Z_{N_2}}{Z_N} \frac{\mathrm{e}^{-\beta H(p_1,q_1)}}{h^{3N} N!}
\end{align*}
%
folgt
%
\begin{align*}
	\mathcal{Z} \mathrm{e}^{-\frac{pV}{\kB T}} &= 1 && |\; \log(\cdot) \\
	\log \mathcal{Z} - \frac{pV}{\kB T} &= 0 \\
	pV &= \kB T \log \mathcal{Z}
\end{align*}

Mit diesem Potential können wir alle anderen Potentiale finden.
%
\begin{align*}
	U
	&= \braket{H} = \sum_N \int \diff^3 p \diff^3 q\ H(p,q)\ \varrho(p,q,N) \\
	&= \frac{1}{\mathcal{Z}} \sum_N z^N \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} H(p,q)\ \mathrm{e}^{-\beta [H(p,q) - \mu N]} \\
	&= \frac{1}{\mathcal{Z}} \sum_N z^N \left( -\frac{\partial}{\partial\beta} \right) \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \mathrm{e}^{-\beta [H(p,q) - \mu N]} \\
	&= \frac{-\frac{\partial}{\partial\beta} \mathcal{Z}}{\mathcal{Z}} \\
	&= \left. -\frac{\partial}{\partial\beta} \log \mathcal{Z} \right|_{V,z} \; , \quad \text{kalorische Zustandsgleichung}
\end{align*}
%
Analog finden wir die Teilchenzahl
%
\begin{align*}
	\braket{N} = \left. + z -\frac{\partial}{\partial z} \log \mathcal{Z}(p,V,z) \right|_{V,\beta}
\end{align*}
%
Weiter können wir $F$ konstruieren, nämlich \[F=U-TS\] wobei $U = \braket{H}$ ist und \[S = \int\limits_{0}^{T} \mathrm{d}T \, \frac{c_V}{T}\] mit $c_V = \frac{\partial U}{\partial T}\bigg|_{V}$.

\minisec{Vergleich}
\begin{itemize}
	\item Mikrokanonisches Ensemble ($N$,$V$,$E$)
		\begin{align*}
			\Gamma(E) &= \int_{E<H(p,q)<E+\Delta} \diff^{3N} p \diff^{3N} q\ \frac{1}{h^{3N} N!} \\
			S(U,V,N) &= \kB \log \Gamma(E)
		\end{align*}
	\item Kanonisches Ensemble ($N$,$V$,$T$)
		\begin{align*}
			Z_N(V,T) &= \int \diff^{3N} p \diff^{3N} q\ \frac{\mathrm{e}^{-\beta H(p,q)}}{h^{3N} N!} \\
			F(T,V,N) &= - \kB T \log \mathcal{Z}
		\end{align*}
	\item Großkanonisches Ensemble ($\mu$,$V$,$T$)
		\begin{align*}
			\mathcal{Z}(V,T,Z) &= \sum_{N=0}^{\infty} z^N Z_N(V,T) \\
			-\Omega(T,V,\mu) &= \kB T \log \mathcal{Z} = pV
		\end{align*}
\end{itemize}

\section{Fluktuationen}

Im folgenden befassen wir uns mit Fluktuationen in der Energie im kanonischen Ensemble und werden feststellen, dass diese klein in extensiven Größen sind.

\subsection{Energiefluktuationen im kanonischen Ensemble}

\begin{align*}
	U &= \braket{H} = \frac{1}{Z_N} \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!}  \mathrm{e}^{-\beta H} H \\
	0 &= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \mathrm{e}^{-\beta (H-F)} (U-H)
\intertext{Ableiten nach $\beta$}
	0 &= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \left\{ - (U-H) \left[ H-F-\beta\frac{\partial F}{\partial\beta} \right] + \frac{\partial U}{\partial\beta} \right\} \mathrm{e}^{-\beta (H-F)} \\
	- \frac{\partial U}{\partial\beta}
	&= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} (U-H)^2 \mathrm{e}^{-\beta(H-F)} \\
	&= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} (\braket{H}-H)^2 \mathrm{e}^{-\beta(H-F)} \\
	&= \braket{(H-\braket{H})^2} = \braket{H^2} - \braket{H}^2
\end{align*}

\begin{align*}
	\frac{\partial U}{\partial\beta}
	&= \frac{\partial U}{\partial T} \frac{\partial T}{\partial\beta}
	= \frac{C_V}{\kB} \frac{\partial \frac{1}{\beta}}{\partial\beta} = - \frac{1}{\kB \beta^2} C_V = -\kB T^2 C_V \\
	\braket{(H-\braket{H})^2} &= \kB T^2 C_V \\
	\frac{\Delta H}{\braket{H}} &= \frac{\sqrt{\braket{(H-\braket{H})^2}}}{\braket{H}} = \frac{\sqrt{\kB T^2 C_V}}{\braket{H}} \sim \frac{\sqrt{N}}{N} \sim \frac{1}{\sqrt{N}} \xrightarrow{N\to\infty} 0
\end{align*}
%
Eine alternative Herleitung ist folgende Betrachtung:
%
\begin{align*}
	Z_N &= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \mathrm{e}^{-\beta H} = \int_0^\infty \diff E\ \omega(E)\ \mathrm{e}^{-\beta E}
\end{align*}

\begin{notice}[Erinnerung:]
	$\aligned[t]
		\omega(E) &= \frac{\partial \Sigma(E)}{\partial E} \\
		\Sigma(E) &= \int_{H<E} \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} = \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \Theta(E-H(p,q)) \\
		\frac{\partial \Sigma(E)}{\partial E} &= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \frac{\partial \Theta(E-H)}{\partial E} \\
		&= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \delta(E-H) = \omega(E)
	\endaligned$
\end{notice}

Damit:
%
\begin{align*}
	Z_N
	&= \int \frac{\diff^{3}p \diff^{3}q}{h^{3N} N!} \mathrm{e}^{-\beta H} \\
	&= \int_0^\infty \diff E\ \omega(E)\ \mathrm{e}^{-\beta E} \\
	&= \int_0^\infty \diff E\ \mathrm{e}^{-\beta [E - \kB T \log\omega(E)]} \\
	&= \int_0^\infty \diff E\ \mathrm{e}^{-\beta [E - T S(E)]} \\
	&\equiv \int_0^\infty \diff E\ \mathrm{e}^{-\beta \mathcal{F}(E)} \\
	\mathrm{e}^{-\beta F}
	&= \int_0^\infty \diff E\ \mathrm{e}^{-\beta \mathcal{F}(E)}
\end{align*}
%
Entwickle:
\[ \mathcal{F}(E) = \mathcal{F}(\tilde{E}) + \frac{1}{2} \left.\frac{\partial^2 \mathcal{F}}{\partial E^2}\right|_{\tilde{E}}(E-\tilde{E})^2 \]
$\tilde{E}$ ist Minimum von $\mathcal{F}(E)$:
\[ \frac{\partial \mathcal{F}}{\partial E} = \frac{\partial}{\partial E} [E-TS(E)] = 1 - T \frac{\partial S}{\partial E} = 0 \]
\[ \frac{\partial^2 \mathcal{F}}{\partial E^2} = - T \frac{\partial^2 S}{\partial E^2} = - T \frac{\partial \frac{1}{T}}{\partial E} = \frac{1}{T C_V} \]

Nach dieser Entwicklung:
%
\begin{align*}
	Z_N
	&= \mathrm{e}^{-\beta F} \approx \mathrm{e}^{-\beta(U-TS(U))} \int_0^\infty \diff E\ \mathrm{e}^{-\frac{(E-U)^2}{2 \kB T^2 C_V}} \\
	&= \mathrm{e}^{-\beta F} \approx \mathrm{e}^{-\beta(U-TS(U))} \sqrt{2 \kB T^2 C_V}
\end{align*}

Der Unterschied zwischen der freien Energie in kanonischen und mikrokanonischen Ensemble ist:
\[ F_\textnormal{kanon.} = F_\textnormal{mikro.} + \kB T \log \sqrt{2 \kB T C_V} \]

