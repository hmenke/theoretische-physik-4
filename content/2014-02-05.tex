% Henri Menke, Michael Schmid 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 5.2.2014
% TeX: Michi

\renewcommand{\printfile}{2014-02-05}

\minisec{Phasenübergänge}

Im folgenden wollen wir am Beispiel der magnetischen Ordnung im transversalen \acct{Ising Modell} Phasenübergänge untersuchen. Dazu gehen wir von Abbildung~\ref{fig:2014-02-05-1} aus. Es gilt
%
\begin{align*}
	\bm{S}_i &= \frac{\hbar}{2} \bm{G} = \frac{\hbar}{2} \begin{pmatrix} G^{x} \\ G^{y}  \\ G^{z} \end{pmatrix}.
\end{align*}
%
\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[step=0.5] (-0.3,-0.3) grid (1.8,1.3);
		\begin{scope}[shift={(0,1)}]
			\draw[->] (45:-0.3) -- (45:0.3);
		\end{scope}
		\begin{scope}[shift={(0.5,0.5)}]
			\draw[->] (45:-0.3) -- (45:0.3);
		\end{scope}
	\end{tikzpicture}
	\caption{Gitter mit Spins nach dem Ising Modell}
	\label{fig:2014-02-05-1}
\end{figure}
%
Hierbei gehen wir von folgenden Hamiltonopertor aus:
%
\begin{align}
	H &= -J \sum\limits_{\braket{i,j}} \sigma_i^z \sigma_j^z + \Omega \sum\limits_{i} \sigma_i^x \label{eq:2014-02-05-1}
\end{align}
%
Für $\Omega \gg J$ handelt es sich hierbei um einen Paramagnet und für $\Omega \ll J$ und $J \gg T$ um einen Ferromagnet. Der Hamiltonoperator~\eqref{eq:2014-02-05-1} besitzt hierbei folgende Symmetrie:
%
\begin{align*}
	\sigma_i^z \to -\sigma_i^z \quad \forall i.
\end{align*}
%
Man spricht auch von einer $\mathbb{Z}_2$ Symmetrie. Ein Ferromagnet bricht diese Symmetrie, d.h. sein Grundzustand ist 2 fach entartet. Er stellt deshalb ein Beispiel für die Symmetriebrechung dar. 
\begin{itemize}
	\item Mathematische rigorose Definition über die Korrelationsfunktionen, d.h.
		%
		\begin{align*}
			\lim\limits_{|i-j|\to \infty} \Braket{\sigma_i^z \sigma_j^z} \to m^2 = \text{const} \neq 0.
		\end{align*}	
		%
		Damit erhalten wir einen Ordnungsparameter $m$.
	\item \acct{Mean-field Näherung} mit folgendem Ansatz:
		%
		\begin{align*}
			\braket{\sigma_j^z} &= m
		\end{align*}
		%
		Vergleiche dazu Abbildung~\ref{fig:2014-02-05-2}, den es zeigt sich, dass bei vielen Nachbarn sich die Fluktuationen herausmitteln.
		%
		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\draw[step=0.5] (-0.8,-0.8) grid (0.8,0.8);
				\draw[MidnightBlue,->] (45:-0.3) -- (45:0.3);
				\foreach \i in {0,90,180,270} {
					\node[DarkOrange3,dot] at (\i:0.5) {};
				}
			\end{tikzpicture}
			\caption{Gitter mit Spins nach dem Ising Modell und der mean-field Näherung}
			\label{fig:2014-02-05-2}
		\end{figure}
		%
		\begin{align*}
			\sigma_i^z \sum\limits_{'j'} \sigma_j^z \to \sigma_i^z \sum\limits_{'j'} \braket{\sigma_j^z} = \sigma_i^z z m 
		\end{align*}
		%
		Damit reduziert sich der Hamiltonoperator zu
		%
		\begin{align*}
			H &= \sum\limits_{i} H_i \\
			H_i &= -Jmz\sigma_i^z + \Omega \sigma_i^x \\
			E^{\pm} &= \pm \sqrt{J^2m^2z^2+\Omega^2} \ket{\pm}
		\end{align*}
		%
	\item Der kanonische Dichteoperator ist 
		%
		\begin{align*}
			\varrho &= \mathrm{e}^{-\beta H} \\
			&= \mathrm{e}^{-\beta \sum\limits_{i} H_i} \\
			&= \prod\limits_{i} \mathrm{e}^{-\beta H_i} \\
			&= \prod\limits_{i} \left( \mathrm{e}^{-\beta E_-} \ket{-}\bra{-} + \mathrm{e}^{-\beta E_+} \ket{+}\bra{+} \right) 
		\end{align*}
		%
	\item Die freie Energie ist 
		%
		\begin{align*}
			F[m] &= k_\mathrm{B} T \log\left(\mathrm{tr}[\varrho]\right) \\
			&= k_\mathrm{B} T \log Z
		\end{align*}
		%
	\item Die Magnetisierung ist eine Hemmung, d.h. der optimale Wert folgt durch Minimierung:
		%
		\begin{align*}
			\partial_m F[m] &= 0 \Leftrightarrow  m = \frac{\mathrm{tr} [\sigma_i^z \varrho]}{\mathrm{tr}[\varrho]}
		\end{align*}
		%
		Wir entwickeln $F[m]$ um kleine $m$ und erhalten
		%
		\begin{align*}
			F[m] & \approx F_0 + \frac{r(T)}{2}m^2 + u m^4+ \ldots
		\end{align*}
		%
		$F$ muss invariant sein unter Symmetrietransformationen, d.h. keine linearen Terme in der Entwicklung, da $m$ nicht entartet. Abbildung~\ref{fig:2014-02-05-3} zeigt ein typisches Bild eines Phasenüberganges.
		%
		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\draw[->] (-2,0) -- (2,0) node[below] {$m$};
				\draw[->] (0,-0.3) -- (0,2.5) node[left] {$F[m]$};
				\draw[DarkOrange3] plot[smooth,domain=-1.7:1.7] (\x,{(\x)^4-2*(\x)^2}) coordinate[label={85:$T< T_C$}];
				\draw[MidnightBlue] plot[smooth,domain=-1.1:1.1] (\x,{2*(\x)^2}) coordinate[label={92:$T> T_C$}];
				\draw[Purple] (1,0.1) -- (1,-0.1) node[below] {$m_0$};
				\draw[Purple] (-1,0.1) -- (-1,-0.1) node[below] {$-m_0$};
				\draw[Purple] (1,-1) node[dot] {} (-1,-1) node[dot] {};
			\end{tikzpicture}
			\caption{Standartbild eines Phasenübergangs 2. Ordnung}
			\label{fig:2014-02-05-3}
		\end{figure}
		%
		Die Korrelationsfunktion hat dann die Form
		%
		\begin{align*}
			\Braket{\sigma_i^z \sigma_j^z} &= \begin{cases}
			1 &, i=j \\
			m^2 &,i\neq j
			\end{cases}
		\end{align*}
		%
		Vergleiche hierzu auch Abbildung~\ref{fig:2014-02-05-4}.
		%
		\begin{figure}[htpb]
			\centering
			\begin{tikzpicture}
				\draw[->] (-0.3,0) -- (4,0) node[below] {$\frac{\Omega}{J z}$};
				\draw[->] (0,-0.3) -- (0,2.5) node[left] {$T$};
				\draw (-0.1,1.5) node[left] {$T_C$} -- (0,1.5) to[bend left] node[dot,pos=0.6] (A) {} (2.5,0) node[dot,label={below:$\Omega_C$}] (B) {};
				\node[right] at (0,0.4) {Ferromagnet};
				\node[right] at (1.5,2.5) {ungeordnete Phase};
				\draw[<-] (A) to[bend left] ++(0.3,0.4) node[right] {konventioneller therm. Übergang};
				\draw[<-] (B) to[bend left] ++(0.3,0.4) node[right] {Quantenphaseübergang};
			\end{tikzpicture}
			\caption{Mean-field Phasenübergang}
			\label{fig:2014-02-05-4}
		\end{figure}
\end{itemize}

\section{Landau Theorie von zweite Ordnung Phasenübergängen}

Wir haben einen Phasenübergang mit einem Ordnungsparameter $O_i$ (bestimmt durch die Symmetrie die gebrochen wird).
\begin{enumerate}
	\item In der Nähe des Phasenübergangs ist der Ordnungsparameter klein und wir entwickeln die 	freie Energie in Ordnungsparameter
		%
		\begin{align*}
			F = F_0 + F(O_i)
		\end{align*}
		%
	\item Es kommen nur Terme vor, die durch die Symmetrie erlaubt sind. Aber alle die erlaubt sind, müssen vorkommen.
	\item Wir erlauben einen ortsunabhängigen Parameter $O_i(\lambda)$ und nehmen die tiefsten Gradiententerme mit.
\end{enumerate}

Die freie Energie ist somit
%
\begin{align*}
	F[\phi(x)] &= \int \left[ \frac{c}{2} \left(\nabla \phi\right)^2 + \frac{r(T)}{2} \phi^2 + u \phi^4 + h \phi \right]
\end{align*}
%
Der Parameter $r$ weist folgende Abhängigkeit auf
%
\begin{align*}
	r(T) &= (T-T_C) r_0.
\end{align*}
% 
Stellt sich noch die Frage nach dem Aussehen von $m(T)$ für $T<T_C$?
%
\begin{align*}
	r_0 (T-T_C) \phi + 4 u \phi^3 &= 0 \\
	\implies \phi & \propto \sqrt{T_C - T} 
\end{align*}
%
Vergleiche hierzu Abbildung~\ref{fig:2014-02-05-5}.
%
\begin{figure}[htpb]
	\centering
	\begin{tikzpicture}
		\draw[->] (-0.3,0) -- (3,0) node[below] {$T$};
		\draw[->] (0,-1) -- (0,1.5) node[left] {$\phi(T)$};
		\draw[rotate=90] plot[domain=-1:1] (\x,{(\x)^2-2}) node[above right] {$\sqrt{T_C-T}$};
		\draw (2,0) -- (2,-0.4) node[below] {$T_C$};
	\end{tikzpicture}
	\caption{Veranschaulichung zu $\phi \propto \sqrt{T_C - T}$, wobei $T_C$ die kritische Temperatur ist, m. a. W. ein Phasensprung}
	\label{fig:2014-02-05-5}
\end{figure}
%
D.h. insbesondere
%
\begin{align*}
	\phi \propto \text{const} (T_C-T)^{\beta}
\end{align*}
%
Innerhalb der >>mean field<< Näherung ist der Exponent \[ \beta = \frac{1}{2}. \]
\begin{notice*}
	Für 3 Dimensionen ist $\beta = 1/3$, d.h. $\beta$ ist abhängig von der Dimension.
\end{notice*}
