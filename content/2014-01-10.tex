% Henri Menke, Michael Schmid 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung am 10.01.2014
% TeX: Michi

\renewcommand{\printfile}{2014-10-01}

\subsection{Ideales Gas im mikrokanonischen Ensemble}
Wir betrachten ein freies Teilchen mit dem Hamiltonoperator
%
\begin{align}
	H &= \frac{1}{2m}\sum\limits_{i}^{N}p_i^2 = E \qquad N,V = \text{const}.
\end{align}
%
Das Phasenraumvolumen $\Sigma(E)$ ist dementsprechend:
%
\begin{align*}
	\Sigma(E) &= \frac{1}{h^{3N}N!} \underbrace{\int \mathrm{d}^3q_1 \ldots \mathrm{d}^3q_N}_{=V^N} \int\limits_{H<E} \mathrm{d}^3p_1 \ldots \mathrm{d}^3p_N 
\end{align*}
%
Die Integration über die $3N$-dimesionalen Kugel mit Radius $\sqrt{\sum\limits_{i}p_i^2} = \sqrt{2mE} \equiv R$ ergibt das Volumen:
%
\begin{align*}
	S^{3N} &= \frac{\pi^{n/2}}{\Gamma\left(\frac{n}{2}+1\right)} R^{n} \bigg|_{n=3N}
\end{align*}
%
\begin{notice}
	Für die Gamma-Funktion gilt
	%
	\begin{align*}
		\Gamma(1/2) & =\sqrt{\pi} \\
		\Gamma(x+1) &= x \Gamma(x) \\
		\Gamma(3/2) &= \Gamma(1/2+1) \\ 
		&= \frac{1}{2} \Gamma(1/2) \\
		&= \frac{\sqrt{\pi}}{2} 
	\end{align*}
	%
\end{notice}

\begin{example}
	Eigenschaften von hoch-dimensionalen Kugeln:
	%
	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\draw (0,0) circle (1);
			\draw (0,0) circle (0.8);
			\draw[->] (0,0) -- node[right] {$R$} (90:1);
			\draw[->] (0,0) -- node[below] {$R-\varepsilon$} (160:0.8);
		\end{tikzpicture}
		\caption{Querschnitt einer Hohlkugel mit Innenradius $R-\varepsilon$ und Außenradius $R$}
		\label{fig:2014-01-10-1}
	\end{figure}
	%
	Mit Abbildung~\ref{fig:2014-01-10-1} veranschaulichen wir uns das Verhalten im $N$-dimensionalen Fall:
	%
	\begin{align*}
		\frac{S^N(R) - S^N(R-\varepsilon)}{S^N(R)} &= 1- \frac{\frac{\pi^{N/2}}{\Gamma(N/2 + 1)} (R-\varepsilon)^N}{\frac{\pi^{N/2}}{\Gamma(N/2 + 1)} R^N} \\
		&= 1- \underbrace{\left(1- \frac{\varepsilon}{R}\right)^N}_{\stackrel{N \to \infty}{\longrightarrow}0}
	\end{align*}
	%
	Bei hochdimensionalen Kugeln ist das gesamte Volumen auf der Oberfläche der Kugel !
\end{example}

Somit erhalten wir
%
\begin{align*}
	\Sigma(E) &= \frac{V^N}{h^{3N} N!} \frac{\pi^{3N/2}}{\Gamma(3N/2+1)}\left(2mE\right)^{3N/2}.
\end{align*}
%
Mit der \acct*{Stirlingschen Formel} \index{Stirling Formel}
%
\begin{align*}
	\Gamma(1+z) &= z^z \mathrm{e}^{-z} \sqrt{2 \pi z} \left(1+ \ldots \right) \\
	N! &= \Gamma(1+N) = N^N \mathrm{e}^{-N} \sqrt{2 \pi N}
\end{align*}
%
finden wir für unser Phasenraumvolumen
%
\begin{align*}
	\Sigma(E) &= \frac{V^N \pi^{3n/2}}{h^{3N} N^N \mathrm{e}^{-N} \sqrt{2 \pi N}} \frac{(2mE)^{3N/2}}{\left(\frac{3N}{2}\right)^{3N/2} \mathrm{e}^{-3N/2} \sqrt{3 \pi N}} \\
	&= \frac{V^N}{h^{3N} N^N} \frac{1}{N \sqrt{6}} \pi^{3N/2 -1} \mathrm{e}^{5N/2} \left(2mE\right)^{3N/2}
\end{align*}
%
und für die Entropie des idealen Gases
%
\begin{align*}
	S(E,V,N) &= k_\mathrm{B} \log \Sigma(E) \\
	&= k_\mathrm{B} N \left[ \log\left(\frac{V}{N}\right) + \frac{3}{2} \log\left(\frac{4mE\pi}{3 h^3 N } \right) + \frac{5}{2} \right]
\end{align*}
%
\begin{notice}
	Dies scheint ein Widerspruch zum 3. HS zu sein. Erst die quantenmechanische Rechnung führt auf das richtige Ergebnis. Der Fehler liegt also an der klassischen Betrachtung. 
\end{notice}
%
Für die innere Energie $U$ erhalten wir somit
%
\begin{align*}
	U(S,V,N) &= \left(\frac{N}{V}\right)^{2/3} \exp\left(\frac{\pi S}{3 N k_\mathrm{B}} - \frac{5}{3}\right) \frac{3Nh^2}{4 \pi m}.
\end{align*}
%
Damit kann leicht die Temperatur und die kalorische Zustandsgleichung gefunden werden
%
\begin{align*}
	T &= \frac{\partial U }{\partial S}\bigg|_{V} \\
	&= \frac{2 U }{3 N k_\mathrm{B}} \\
	\implies U &= \frac{3}{2} N k_\mathrm{B} T. 
\end{align*}
%
Mit der spezifischen Wärme
%
\begin{align*}
	C_V &= \frac{\partial U}{\partial T} = \frac{3}{2} N k_\mathrm{B}
\end{align*}
%
erhalten wir für die Entropie den bekannten Ausdruck
%
\begin{align*}
	S(T,V,N) &= N k_\mathrm{B} \log\left(\frac{V}{N}\right) + C_V \log T + \text{const}
\end{align*}
%
Die thermische Zustandsgleichung folgt aus
%
\begin{align*}
	p &= - \frac{\partial U }{\partial V}\bigg|_{S} = \frac{2 U}{3 V} = \frac{N k_\mathrm{B} T}{V}.
\end{align*}
%
Damit haben wir die Zustandsgleichung des idealen Gases mikroskopisch hergeleitet.

\subsection{Äquipartitionsgesetz} 
Im folgenden leiteten wir mittels der statistischen Mechanik das \acct{Äquipartitionsgesetz} im mikrokanonischen Ensemble her. Dieses besagt, dass der Erwartungswert der Produkte $x_i\partial H / \partial x_j$ wobei $x_i,x_j \in \left\{p_i,q_i \right\}$ gegeben ist durch
%
\begin{align}
	\Braket{x_i \frac{\partial H}{\partial x_j}} &= \delta_{ij} k_\mathrm{B} T.
\end{align}
% 
Für das ideale Gas mit $H = \sum p_i^2/2m$ ergibt sich
%
\begin{align*}
	\Braket{\frac{p_i^2}{2m}} &= \frac{k_\mathrm{B}T}{2},
\end{align*}
%
d.h., im Gleichgewicht trägt jeder Freiheitsgrad der Bewegung die Energie $k_\mathrm{B}T/2$. Für die Gesamtenergie bedeutet das
%
\begin{align*}
	U &= \braket{H} \\ 
	&= \Braket{\sum_i \frac{p_i^2}{2m}} \\
	&= \sum\limits_{i=1}^{N}\sum\limits_{\alpha = x,y,z} \Braket{\frac{p_{\alpha i}^2}{2m}} \\
	&= \frac{3N}{2m} \Braket{p_{x1}^2} \\
	\implies   \Braket{p_{x1}^2} &= m T k_\mathrm{B}
\end{align*}
%
\begin{example}
	\begin{itemize}
		\item 	Für ein freies Gas mit $x_i = p_{1x}$ folgt
			%
			\begin{align*}
				\frac{\partial H}{\partial x_i} &= \frac{p_{x1}}{m} \implies \Braket{p_{1x}^2 \frac{1}{m}} = k_\mathrm{B} T
			\end{align*}
			%
		\item Für einen harmonischen Oszillator gilt:
			%
			\begin{align*}
				\Braket{\frac{p_i^2}{2m}} = \Braket{\frac{m}{2} \omega^2 q_i^2} = \frac{1}{2} k_\mathrm{B}T
			\end{align*}
			%
	\end{itemize}
\end{example}

\begin{proof}
	Wir betrachten folgende Umformung
	%
	\begin{align*}
		\int\limits_{H<E} x_j \frac{\partial H}{\partial x_i} \, \mathrm{d}p \mathrm{d}q &= \int\limits_{H<E} x_y \frac{\partial (H-E)}{\partial x_i} \, \mathrm{d}p\mathrm{d}q \\
		&=\underbrace{\int\limits_{H<E} \frac{\partial}{\partial x_i} \left[x_j (H-E)\right] \, \mathrm{d}q \mathrm{d}p }_{= 0}- \delta_{ij} \int\limits_{H<E} (H-E) \, \mathrm{d}q \mathrm{d}p
	\end{align*}
	%
	Im letzten Schritt wurde die partielle Integration verwendet. Es zeigt sich außerdem, dass das linke Integral auf der rechten Seite verschwindet, da am Rand $H=E$ gelten soll. Damit finden wir
	%
	\begin{align*}
		\Braket{x_j \frac{\partial H}{\partial x_i}} &= \frac{1}{\Gamma} \int\limits_{E<H<E+\Delta} x_j \frac{\partial H}{\partial x_i} \, \mathrm{d}p \mathrm{d}q \\
		&= \frac{\Delta }{\Gamma} \frac{\partial}{\partial E} \left\{ \int\limits_{H<E} x_j \frac{\partial H}{\partial x_i} \, \mathrm{d}p \mathrm{d}q \right\} \\
		&= \frac{\Delta }{\Gamma} \left(-\delta_{ij}\right) \frac{\partial}{\partial E} \int\limits_{H<E} (H-E) \, \mathrm{d}p \mathrm{d}q \\
		&= \frac{\Delta}{\Gamma} \int \delta_{ij} + 0 \, \mathrm{d}p \mathrm{d}q \\
		&= \frac{\delta_{ij}}{\partial_E \Sigma(E)} \Sigma(E) \\
		&= \frac{\delta_{ij}}{\partial_E \log \Sigma(E)} \\
		&= \frac{\delta_{ij} k_\mathrm{B}}{\partial_E S(E,V,N)} \\
		&= \delta_{ij} k_\mathrm{B} T
	\end{align*}
	%
\end{proof}

