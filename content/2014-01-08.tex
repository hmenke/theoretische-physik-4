% Henri Menke, Michael Schmid, Marcel Klett, Jan Schnabel 2013 Universität Stuttgart.
%
% Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
% Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Deutschland
% zugänglich. Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
% http://creativecommons.org/licenses/by-nc-sa/3.0/de/ oder wenden Sie sich
% brieflich an Creative Commons, 444 Castro Street, Suite 900, Mountain View,
% California, 94041, USA.

% Vorlesung vom 08.01.2014
% TeX: Henri

\renewcommand{\printfile}{2014-01-08}

\chapter{Klassische Statistische Mechanik}

Ein System von $N$ Teilchen besitzt $6 N$ Freiheitsgrade.
\[ (p,q) = (\bm{p}_1,\dotsc,\bm{p}_N,\bm{q}_1,\dotsc,\bm{q}_N) \]
Die Teilchen sind verteilt mit der Dichte $\varrho(p,q)$. Es handelt sich also um das Konzept des $6N$ dimensionalen Phasenraumes $\Gamma$, dessen Punkte $(p,q)$ den Zustand des Systems beschreiben.

Für Messgrößen $\mathcal{M}(p,q)$ gilt der Erwartungswert
%
\begin{align*}
	\braket{\mathcal{M}} = \frac{\int \diff p \diff q\ \varrho(p,q) \mathcal{M}(p,q)}{\int \diff p \diff q\ \varrho(p,q)}
\end{align*}

\minisec{Ensembles}

\begin{description}
	\item[Mikrokanonisches Ensemble] $N,V,E$ fixiert (abgeschlossenes System)
	\item[Kanonisches Ensemble] $N,V$ fixiert (System im Kontakt mit einem Reservoir, Energieübertrag möglich)
	\item[Großkanonische Ensemble] $V$ fixiert (System im Kontakt mit einem Reservoir, mit dem Energie ausgetauscht werden kann und einem Reservoir mit dem Teilchen ausgetauscht werden können)
\end{description}

\section{Mikrokanonisches Ensemble} \index{Ensemble!klassisch mikrokanonisch}

Die statistische Mechanik basiert auf dem Postulat gleicher Wahrscheinlichkeit für jeden Zustand im Phasenraum der mit festem $E,V,N$ verträglich ist.
%
\begin{align*}
	\varrho(p,q) &=
	\begin{cases}
		0 & \text{sonst}\\
		\frac{1}{h^{3N} N!} & E < H(p,q) < E + \Delta \\
	\end{cases}
\end{align*}

Der Faktor $N!$ beruht auf einem Ansatz von Gibbs und löst das Gibbsche Paradoxon. Wir definieren das durch das mikrokanonische Ensemble besetzte Volumen im Zustandsraum.
%
\begin{gather*}
	\Gamma(E) = \int \diff^{3N}p\diff^{3N}q\ \varrho(p,q) = \int_{E<H(p,q)<E+\Delta} \diff^{3N}p\diff^{3N}q\ \frac{1}{h^{3N} N!} \\
	\left.
		\aligned
			\Sigma(E) &= \int_{H(p,q)<E} \diff^{3N}p\diff^{3N}q\ \frac{1}{h^{3N} N!} \\
			\omega(E) &= \frac{\diff \Sigma(E)}{\diff E}
		\endaligned
	\right\}
	\Gamma(E) = \Sigma(E+\Delta) - \Sigma(E) \simeq \omega(E) \cdot \Delta
\end{gather*}

$\omega(E)$ ist hierbei die Zustandsdichte. Es zeigt sich, dass die Entropie bestimmt ist durch
\[ S(E,V,N) = \kB \log\Gamma(E) \]
\begin{itemize}
	\item $S$ ist extensiv in $E,V,N$
	\item $S$ ist maximal im abgeschlossenen System
\end{itemize}
Aufgrund dieser Eigenschaften interpretieren wir die Größe $\kB \log\Gamma(E)$ als Entropie. Damit erkennen wir die Bedeutung der Entropie. Sie zählt die zur Verfügung stehenden Zustände im $\Gamma$-Raum und steuert das System in Richtung maximaler Unordnung.

\begin{proof}
	dass $S$ extensiv ist.

	Wir teilen das System in zwei Teile mit $E_1,V_1,N_1$ und $E_2,V_2,N_2$.
	Dann haben die isolierten Systeme die Entropie
	%
	\begin{align*}
		S_1(E_1,V_1,N_1) &= \kB \log\Gamma_1(E_1) \\
		S_2(E_2,V_2,N_2) &= \kB \log\Gamma_2(E_2)
	\end{align*}

	\begin{figure}[htpb]
		\centering
		\begin{tikzpicture}
			\begin{scope}
				\draw[->] (-0.3,0) -- (4,0) node[below] {$q_1$};
				\draw[->] (0,-0.3) -- (0,4) node[right] {$p_1$};
				\draw (0.5,0.5) .. controls (1,1) .. (3.5,2) node[right] {$0$};
				\draw (0.5,2) .. controls (1,2.5) .. (3.5,3.5) node[right] {$E$};
				\filldraw[fill=lightgray] (0.5,1.5) .. controls (1,2) .. (3.5,3) node[right] {$E_1 + \Delta$} -- (3.5,2.7) node[right] {$E_1$} .. controls (1,1.7) .. (0.5,1.2) -- cycle;
			\end{scope}

			\begin{scope}[xshift=5cm]
				\draw[->] (-0.3,0) -- (4,0) node[below] {$q_2$};
				\draw[->] (0,-0.3) -- (0,4) node[right] {$p_2$};
				\draw (0.5,0.5) .. controls (0.5,1) .. (1.4,3.5) node[above] {$E$};
				\draw (2.5,0.5) node[right] {$0$} .. controls (2.5,1) .. (3.5,3.5);
				\filldraw[fill=lightgray] (1.8,0.5) node[below left] {$E_2 + \Delta$} .. controls (1.8,1) .. (2.8,3.5) node[above right] {$E_2$} -- (2.5,3.5) .. controls (1.5,1) .. (1.5,0.5) -- cycle;
			\end{scope}
		\end{tikzpicture}
		\caption{Verteilung des Volumens $\Gamma$ im Phasenraum.}
		\label{fig:2014-01-08-1}
	\end{figure}

	Das gesamte System mit Hemmungen besitzt das Phasenraumvolumen
	\[ \Gamma_1(E_1) \Gamma_2(E_2) \]
	Es muss gelten
	\[ E < E_1 + E_2 < E + \Delta \]
	Als nächstes relaxieren wir die Hemmung an der Energie
	\[ \Gamma(E) = \sum_{E<E_1+E_2<E+\Delta} \Gamma_1(E_1) \Gamma_2(E_2) = \sum_{i}^{E/\Delta} \Gamma_1(E_{1i}) \Gamma_2(E-E_{1i}) \]
	mit $E_{1i} = \Delta \cdot i$.
	\[ \Gamma(\bar{E}_1) \Gamma(\bar{E}_2) < \Gamma(E) < \frac{E}{\Delta} \Gamma(\bar{E}_1) \Gamma(\bar{E}_2) \]
	Dabei sind $\bar{E}_1$ und $\bar{E}_2$ so definiert, dass $\Gamma_1(\bar{E}_1) \Gamma_2(\bar{E}_2)$ maximal ist.
	\begin{align*}
		S(E,V,N)
		&= \kB \log\Gamma(E) \leq \kB \log \frac{E}{\Delta} \Gamma_1(\bar{E}_1) \Gamma_2(\bar{E}_2) \\
		&= \kB \log \frac{E}{\Delta} + \kB \log \Gamma_1(\bar{E}_1) + \kB \log \Gamma_2(\bar{E}_2) \\
		&= \log N + S_1 + S_2
	\end{align*}
	Im thermodynamischen Limes ($N\to\infty$) können wir den Term $\log N$ vernachlässigen.
\end{proof}

Der Hauptbeitrag zur Entropie kommt von $\bar{E}_1$ und $\bar{E}_2$
\begin{align*}
	\delta[\Gamma_1(E_1) \Gamma_2(E_2)] |_{\bar{E}_1,\bar{E}_2} &= 0 \\
	\frac{\partial}{\partial E_1} \log \Gamma_1 |_{\bar{E}_1} &= \frac{\partial}{\partial E_2} \log \Gamma_2 |_{\bar{E}_2} \\
	\frac{1}{T_1} = \frac{\partial S_1}{\partial E_1} &= \frac{\partial S_2}{\partial E_2} = \frac{1}{T_2}
\end{align*}
%
Wir definieren
%
\begin{align*}
	T = \frac{1}{\left. \frac{\partial S}{\partial E} \right|_{V,N}}
\end{align*}

$S$ ist maximal in allen Hemmungen des Systems. Mit der Identifizierung $U=E$ erhalten wir die gesamte Thermodynamik.
%
\begin{align*}
	S(U=E,V,N) &= \kB \log \Gamma(E) \\
	U(S,V,N) \text{ folgt aus invertieren von $S$}
\end{align*}

\begin{notice}[Erinnerung:]
	\begin{align*}
		p &= - \left. \frac{\partial U}{\partial V} \right|_{S,N} \\
		\mu &= \left. \frac{\partial U}{\partial N} \right|_{S,V} \\
		T &= \left. \frac{\partial U}{\partial S} \right|_{V,N}
	\end{align*}
\end{notice}
